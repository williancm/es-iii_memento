# Memento
 Memento é um padrão de projeto de software documentado no Catálogo Gang of Four, 
 sendo considerado como um padrão comportamental. 
 Ele permite armazenar o estado interno de um objeto em um determinando momento,
 para que seja possível retorná-lo a este estado, sem que isso cause problemas com o encapsulamento.