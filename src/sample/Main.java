package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.domain.CarroArmazenador;
import sample.domain.CarroOriginador;
import sample.domain.Memento;

import java.util.Scanner;

import static javafx.application.Application.launch;

public class Main extends Application {

    private Stage primaryStage;

    private static Scanner s = new Scanner(System.in);

    private static Memento memento = new Memento();
    private static CarroOriginador carroOriginador = new CarroOriginador();
    private static CarroArmazenador carroArmazenador = new CarroArmazenador();

    public static void main(String[] args) {
//        test();
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Exemplo Memento ");
        primaryStage.setScene(new Scene(root, 360, 450));
        primaryStage.show();
    }

//    public static void test() {
//        int op = 0;
//         while (op != 6) {
//                switch (op = menu()) {
//                    case 1:
//                        novo();
//                        System.out.println("\n\n");
//                        break;
//                    case 2:
//                        verAtual();
//                        System.out.println("\n\n");
//                        break;
//                    case 3:
//                        verOriginal();
//                        System.out.println("\n\n");
//                        break;
//                    case 4:
//                        verTodos();
//                        System.out.println("\n\n");
//                        break;
//                    case 5:
//                        verById();
//                        System.out.println("\n\n");
//                        break;
//                    case 55:
//                        retornar();
//                        System.out.println("\n\n");
//                        break;
//                    case 6:
//                        break;
//
//                    default:
//                        System.out.println("Informe uma opção válida!!!");
//                        break;
//                }
//            }
//        }
//
//    private static void retornar() {
//        if(carroArmazenador.size() > 1){
//            verAtual();
//            System.out.println("");
//            System.out.println("Carro Versão Anterior: ");
//            memento = carroArmazenador.returnUltimoEstadoSalvo();
//            System.out.print("Cor:   " + memento.getCor());
//            System.out.print("\nFarol: " + memento.getFarol());
//            System.out.print("\nRoda:  " + memento.getRoda());
//        } else {
//            System.out.println("Não existem duas versões do Carro no momento!!");
//        }
//    }
//
//    private static void verOriginal() {
//        if(carroArmazenador.size() > 0){
//            System.out.println("Carro Original: ");
//            Memento meAux = carroArmazenador.getOriginal();
//            System.out.print("Cor:   " + meAux.getCor());
//            System.out.print("\nFarol: " + meAux.getFarol());
//            System.out.print("\nRoda:  " + meAux.getRoda());
//        } else {
//            System.out.println("Não existe nenhuma versão do Carro no momento");
//        }
//    }
//
//    private static void verTodos() {
//        if(carroArmazenador.size() > 0){
//            for (int i = 1; i <= carroArmazenador.size(); i++) {
//                System.out.println("Carro Versão: " + i);
//                if(carroArmazenador.size() == i){
//                    System.out.println("ATUAL");
//                }
//                Memento meAux = carroArmazenador.getEstadoById(i-1);
//                System.out.print("Cor:   " + meAux.getCor());
//                System.out.print("\nFarol: " + meAux.getFarol());
//                System.out.print("\nRoda:  " + meAux.getRoda());
//                System.out.println();
//            }
//        } else {
//            System.out.println("Não existe nenhuma versão do Carro no momento");
//        }
//    }
//
//    private static void verById() {
//        if(carroArmazenador.size() > 0){
//            System.out.print("Informe o numero da versão que desejada: (entre 1 e " + carroArmazenador.size() + ")");
//            int num = Integer.parseInt(s.nextLine());
//            while(num-1 >= carroArmazenador.size()){
//                System.out.println("Número inválido!! Informe um número entre 1 e " + carroArmazenador.size() + " !");
//                System.out.print("Informe o numero da versão que desejada: ");
//                num = Integer.parseInt(s.nextLine());
//            }
//            System.out.println("Carro Versão: " + num);
//            Memento meAux = carroArmazenador.getEstadoById(num-1);
//            System.out.print("Cor:   " + meAux.getCor());
//            System.out.print("\nFarol: " + meAux.getFarol());
//            System.out.print("\nRoda:  " + meAux.getRoda());
//        } else {
//            System.out.println("Não existe nenhuma versão do Carro no momento");
//        }
//    }
//
//    private static void verAtual() {
//        if(carroArmazenador.size() > 0){
//            System.out.println("Carro Atual:");
//            System.out.print("Cor:   " + memento.getCor());
//            System.out.print("\nFarol: " + memento.getFarol());
//            System.out.print("\nRoda:  " + memento.getRoda());
//        } else {
//            System.out.println("Não existe nenhuma versão do Carro no momento");
//        }
//    }
//
//    private static void novo() {
//        System.out.println("Criando/Alterando CARRO: ");
//        System.out.print("Digite a Cor do Carro:   ");
//        String cor = s.nextLine();
//        carroOriginador.setCor(cor);
//        System.out.print("Digite o Farol do Carro: ");
//        String farol = s.nextLine();
//        carroOriginador.setFarol(farol);
//        System.out.print("Digite a Roda do Carro:  ");
//        String roda = s.nextLine();
//        carroOriginador.setRoda(roda);
//
////        System.out.println(carroOriginador.getCor());
////        System.out.println(carroOriginador.getRoda());
////        System.out.println(carroOriginador.getFarol());
//
//        memento = carroOriginador.getCarroOriginator(carroOriginador);
//        carroArmazenador.adicionarMemento(memento);
//
//    }
//
//    private static int menu() {
//        System.out.println("MENU:");
//        System.out.println("1 - Criar/Alterar carro");
//        System.out.println("2 - Ver estado atual");
//        System.out.println("3 - Ver estado original");
//        System.out.println("4 - Ver todos os estados");
//        System.out.println("5 - Ver estado especificado por um numero");
//        System.out.println("55- Retornar ao estado anterior;");
//        System.out.println("6 - Sair");
//        System.out.print("Informe a opção: ");
//
//        int escolha = Integer.parseInt(s.nextLine());
//        return escolha;
//    }

}
