package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import sample.domain.CarroArmazenador;
import sample.domain.CarroOriginador;
import sample.domain.Memento;


public class Controller {

    @FXML
    TextField tfCor;
    @FXML
    TextField tfFarol;
    @FXML
    TextField tfRoda;
    @FXML
    TextArea txtArea;
    @FXML
    TextField tfOption;
    @FXML
    Button btnListarTodos;
    @FXML
    Button btnPrimeiro;
    @FXML
    Button btnAtual;
    @FXML
    Button btnVersao;


    CarroArmazenador carroArmazenador = new CarroArmazenador();
    Memento memento;

    @FXML
    private void btnLimpar(){
        txtArea.setText("");
    }

    @FXML
    private void btnSalvar(){
        if(tfCor.toString() == "" || tfCor == null || tfFarol.toString() == "" || tfFarol == null || tfRoda.toString() == "" || tfRoda == null){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Alerta");
            alert.setHeaderText(null);
            alert.setContentText("Preencha pelo menos um campo para poder criar uma nova versão do carro!");

            alert.showAndWait();
        }

            CarroOriginador carroOriginador = new CarroOriginador(tfCor.toString(),
                    tfFarol.toString(),
                    tfRoda.toString());

//            memento.getCarroAtual(carroOriginador);

            carroArmazenador.adicionarMemento(memento);


            txtArea.setText("Cor: "     + memento.getCor()
                         +  "\nFarol: " + memento.getFarol()
                         +  "\nRoda: " + memento.getRoda());

    }


}
