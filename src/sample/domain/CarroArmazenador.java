package sample.domain;

import java.util.ArrayList;

public class CarroArmazenador {
    protected ArrayList<Memento> estados;

    public CarroArmazenador() {
        estados = new ArrayList<Memento>();
    }

    public int size(){
        return estados.size();
    }

    public void adicionarMemento(Memento memento) {
        estados.add(memento);
    }

    public Memento returnUltimoEstadoSalvo() {
        if (estados.size() <= 0) {
            return new Memento("","","");
        }
        Memento estadoSalvo = estados.get(estados.size() - 2);
        estados.remove(estados.size() - 1);
        return estadoSalvo;
    }

    public Memento getEstadoById(int id) {
        if (estados.size() <= 0) {
            return new Memento("","","");
        }
        Memento estadoSalvo = estados.get(id);
        return estadoSalvo;
    }

    public ArrayList<Memento> getAllEstados() {
        if (estados.size() <= 0) {
            return null;
        }
        return estados;
    }

    public Memento getOriginal(){
        Memento memento;
        memento = estados.get(0);
        return memento;
    }

}
