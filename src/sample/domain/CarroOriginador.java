package sample.domain;

public class CarroOriginador {
    protected String cor;
    protected String farol;
    protected String roda;
    CarroArmazenador carroArmazenador = new CarroArmazenador();

    public CarroArmazenador getCarroArmazenador() {
        return carroArmazenador;
    }

    public void setCarroArmazenador(CarroArmazenador carroArmazenador) {
        this.carroArmazenador = carroArmazenador;
    }

    public CarroOriginador(String cor, String farol, String roda) {
        this.cor = cor;
        this.farol = farol;
        this.roda = roda;
    }

    public CarroOriginador() {
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getFarol() {
        return farol;
    }

    public void setFarol(String farol) {
        this.farol = farol;
    }

    public String getRoda() {
        return roda;
    }

    public void setRoda(String roda) {
        this.roda = roda;
    }

    public Memento getCarroOriginator(CarroOriginador co) {
        Memento memento = new Memento();
        memento.cor = co.getCor();
        memento.farol = co.getFarol();
        memento.roda = co.getRoda();
        return memento;
    }

}
