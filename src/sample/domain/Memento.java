package sample.domain;

public class Memento {
    protected String cor;
    protected String farol;
    protected String roda;

    public Memento(String cor, String farol, String roda) {
        this.cor = cor;
        this.farol = farol;
        this.roda = roda;
    }

    public Memento() {
    }

    public String getCor() {
        return cor;
    }

    public String getFarol() {
        return farol;
    }

    public String getRoda() {
        return roda;
    }


//    public Memento getCarroOriginator(CarroOriginador co) {
//        Memento memento = new Memento();
//        memento.cor = co.getCor();
//        memento.farol = co.getFarol();
//        memento.roda = co.getRoda();
//        return memento;
//    }
}
